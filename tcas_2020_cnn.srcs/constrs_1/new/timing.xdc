#https://www.xilinx.com/support/package-pinout-files/

#Virtex 7 Clock pin
#set_property -dict {PACKAGE_PIN E3 IOSTANDARD LVCMOS33} [get_ports clk_i]   

#Artix 7 100t Clock pin
#set_property -dict {PACKAGE_PIN AJ32 IOSTANDARD LVCMOS18} [get_ports clk_i]

#Artix 7 200t 676 Clock pin
set_property -dict {PACKAGE_PIN G5 IOSTANDARD LVCMOS18} [get_ports clk_i] 
create_clock -period 10.000 -name clk_i -waveform {0.000 5.000} -add [get_ports clk_i]