`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UMBC-EEHPC
// Engineer: Nitheesh Manjunath 
// 
// Create Date: 05/09/2020 08:15:54 PM
// Design Name: 
// Module Name: OM_wrapper
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module OM_wrapper#(
    dw = 24,
    aw = 8,
    awFM = 8,
    PE = 4,
    memFile = ""
    )(
    clk, rst_n, Input, nextRow, nextCol, write, convDone, firstConv, Output_o, OM2FMAddress_o, isLoading, doneLoading
    ); 
    
localparam wrEnWidth = $clog2(PE);

input clk;
input rst_n;
input [PE*dw-1:0] Input;
input write;            //Delayed by one cycle to read the stored data in the memory
input nextRow;
input nextCol;
input convDone;
input firstConv;
output [PE*dw-1:0] Output_o;
output reg [awFM-1:0] OM2FMAddress_o;
output reg doneLoading, isLoading;


localparam [0:0] state_0 = 0,
                 state_1 = 1;
reg [0:0] currentState;

//(*keep = "true"*)reg [PE*dw-1:0] Input_2;
(*keep = "true"*)reg read, write_1;
(*keep = "true"*)reg [aw-1:0] readAddress, startAddress, OM2FMAddress;
wire [aw-1:0] readAddressMem;


wire [dw*PE-1:0] Output_1;
wire [PE*dw-1:0] Input_1;
    
genvar i;
generate
    for(i=0; i<PE; i=i+1) begin
        bram_memory_2_port#(
             .dw(dw),
             .aw(aw), 
             .memFile(memFile)
             )OM(
             .clk(clk),
             .data_in(Input_1[dw*(i+1)-1:dw*i]),
             .readAddr(readAddressMem), //[aw*(i+1)-1:aw*i]
             .writeAddr(readAddress),
             .wr(write_1),
             .rd(write | (currentState==state_1) ),
             .data_out(Output_1[dw*(i+1)-1:dw*i]) //[dw*(k+1)-1:dw*k]
            );
        end
endgenerate

genvar j;
generate 
    for(j=0; j<PE; j=j+1) begin
        assign Input_1[dw*(j+1)-1:dw*j] = !firstConv ? Output_1[dw*(j+1)-1:dw*j] + Input[dw*(j+1)-1:dw*j] : Input[dw*(j+1)-1:dw*j];
    end
endgenerate


always@(posedge clk) begin
//    Input_2 <= Input_1;
    write_1 <= write;
end

always@(posedge clk or negedge rst_n) begin
    if(!rst_n || doneLoading) begin
        startAddress <= 0;
    end
    else begin
        if(nextRow) begin
            startAddress <= readAddress;
        end
    end
end

always@(posedge clk or negedge rst_n) begin
    if(!rst_n || doneLoading) begin
        readAddress <= 0;
    end
    else begin
        casez({nextRow,nextCol, write_1})
            3'b000 : readAddress <= readAddress;
            3'b001 : readAddress <= readAddress + 1;
            3'b010 : readAddress <= startAddress;
            3'b011 : readAddress <= startAddress;
            3'b110 : readAddress <= readAddress;
            3'b100 : readAddress <= readAddress;
        endcase
    end
end

always@(posedge clk or negedge rst_n) begin
    if(!rst_n) begin
        currentState <= state_0;
        OM2FMAddress <= 0;
        doneLoading <= 0;
        isLoading <= 0;
    end
    else begin
        case(currentState) 
            state_0 : begin
                OM2FMAddress <= 0;
                doneLoading <= 0;
                isLoading <= 0;
                if(convDone) begin
                    currentState <= state_1;
                end
                else begin
                    currentState <= state_0;
                end
            end
            state_1 : begin
                
                if(OM2FMAddress >= readAddress) begin
                    currentState <= state_0;
                    OM2FMAddress <= 0;
                    doneLoading <= 1;
                    isLoading <= 0;
                end
                else begin
                    currentState <= state_1;
                    OM2FMAddress <= OM2FMAddress + 1;
                    isLoading <= 1;
                end
            end
        endcase
    end
end

always@(posedge clk) begin
    OM2FMAddress_o <= OM2FMAddress[awFM-1:0];
end

assign readAddressMem = (currentState == state_0) ? readAddress: OM2FMAddress;
assign Output_o = Output_1;

endmodule
