`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UMBC-EEHPC
// Engineer: Nitheesh Manjunath
// 
// Create Date: 05/04/2020 08:03:40 PM
// Design Name: 
// Module Name: output_router
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:Fully Async, IF critical path is in this module please change it to sync and non-blocking assignment
//  I think it' not required. Only the Router can be made sync
//////////////////////////////////////////////////////////////////////////////////


module output_router
    #(
    dw = 8,
    PE = 4
    )(
    r_in, en, ready, in0, in1, out
    );

input r_in, en;
input [PE*dw-1:0] in0;
input [PE*dw-1:0] in1;

output reg ready;
output reg [PE*dw-1:0] out;
    
always@(*) begin
    case({en,r_in}) 
        2'b10 : begin
            out = in0;
            ready = 1;
        end
        2'b11 : begin
            out = in1;
            ready = 1;
        end
        default : begin
            ready = 0;
            out = 0;
        end
    endcase
end

endmodule
