`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UMBC-EEHPC
// Engineer: Nitheesh Manjunath 
// 
// Create Date: 05/05/2020 12:11:45 PM
// Design Name: 
// Module Name: WM_wrapper
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module WM_wrapper#(
    dw = 8,
    aw = 8,
    PE = 4,
    memFile = ""
    )(
    clk, rst_n, Input, read, nextMemoryWrite, write, writeAddress, readAddress, Output
    );
    
localparam wrEnWidth = $clog2(PE);

input clk;
input rst_n;

input [dw-1:0] Input;
input read;
input nextMemoryWrite;
input write;
input [aw-1:0] writeAddress;
input [aw-1:0] readAddress;

output [PE*dw-1:0] Output;
    
    
(*keep = "true"*) reg [aw-1:0] wmAddress;
wire [PE-1:0] wrEnBit;
(*keep = "true"*) reg [wrEnWidth-1:0] wrEn;
(*keep = "true"*) reg [dw-1:0] Input_1;
(*keep = "true"*) reg read_1;

n_bit_decoder#(
    .PE(PE),
    .log2_PE(wrEnWidth)
    )decoder(
    .en(write),
    .i(wrEn),
    .out(wrEnBit)
    );

genvar i;
generate
    for(i=0; i<PE; i=i+1) begin : WM 
        bram_memory#(
             .dw(dw),
             .aw(aw), 
             .memFile(memFile)
             )WM(
             .clk(clk),
             .data_in(Input_1),
             .addr(wmAddress),
             .wr(wrEnBit[i]),
             .rd(read_1),
             .data_out(Output[dw*(i+1)-1:dw*i]) //[dw*(k+1)-1:dw*k]
        );
    end
endgenerate

always@(posedge clk) begin
        Input_1 <= Input;
        read_1 <= read;
end

always@(posedge clk or negedge rst_n) begin
     if(!rst_n) begin
        wrEn <= {wrEnWidth{1'b1}};
     end
     else begin
        if(nextMemoryWrite && write) begin
            wrEn <= wrEn + 1;
        end
        else begin
            wrEn <= wrEn;
        end
     end   
end

always@(posedge clk or negedge rst_n) begin
    if(!rst_n) begin
        wmAddress <= 0;
    end
    else begin
        if(write) begin
            wmAddress <= writeAddress;
        end
        else begin
            wmAddress <= readAddress;
        end
    end
end
endmodule

