`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UMBC-EEHPC
// Engineer: Nitheesh Manjunath 
// 
// Create Date: 05/05/2020 12:10:10 PM
// Design Name: 
// Module Name: FM_wrapper
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module FM_wrapper#(
    dw = 32,
    aw = 8,
    PE = 4,
    memFile = ""
    )(
    clk, rst_n, Input, writeAddress, nextMemoryWrite, write, writeAll, read, reado, rdEn, readAddress, Output
    ); 
    
localparam wrEnWidth = $clog2(PE);

input clk;
input rst_n;
input [PE*dw-1:0] Input;
input [aw-1:0] writeAddress;
input nextMemoryWrite;
input write;
input writeAll;
input read;
input reado;
input [$clog2(PE)-1:0] rdEn;
input [PE*aw-1:0] readAddress;

output [PE*dw-1:0] Output;

(*keep = "true"*) reg [PE*aw-1:0] fmAddress;
(*keep = "true"*) reg [PE*dw-1:0] Input_1;
(*keep = "true"*) reg read_1, writeAll_1;
(*keep = "true"*) reg [wrEnWidth-1:0] wrEn;

wire [PE-1:0] wrEnBit, rdEnBit;

n_bit_decoder#(
    .PE(PE),
    .log2_PE(wrEnWidth)
    )wr_decoder(
    .en(write),
    .i(wrEn),
    .out(wrEnBit)
    );
    
n_bit_decoder#(
    .PE(PE),
    .log2_PE(wrEnWidth)
    )rd_decoder(
    .en(reado),
    .i(rdEn),
    .out(rdEnBit)
    );

    
genvar i;
generate
    for(i=0; i<PE; i=i+1) begin : FM 
        bram_memory#(
             .dw(dw),
             .aw(aw), 
             .memFile(memFile)
             )FM(
             .clk(clk),
             .data_in(Input_1[dw*(i+1)-1:dw*i]),
             .addr(fmAddress[aw*(i+1)-1:aw*i]),
             .wr(wrEnBit[i] | writeAll_1),
             .rd(rdEnBit[i] | read_1),
             .data_out(Output[dw*(i+1)-1:dw*i]) //[dw*(k+1)-1:dw*k]
            );
        end
endgenerate

always@(posedge clk) begin
        read_1 <= read;
        Input_1 <= Input;
        writeAll_1 <= writeAll;
end

always@(posedge clk or negedge rst_n) begin
    if(!rst_n) begin
        fmAddress <= 0;
    end
    else begin
        if(write | writeAll) begin
            fmAddress <= {PE{writeAddress}};
        end
        else begin
            fmAddress <= readAddress;
        end
    end
end

always@(posedge clk or negedge rst_n) begin
     if(!rst_n) begin
        wrEn <= {wrEnWidth{1'b1}};
     end
     else begin
        if(nextMemoryWrite && write) begin
            wrEn <= wrEn + 1;
        end
        else begin
            wrEn <= wrEn;
        end
     end   
end

endmodule
