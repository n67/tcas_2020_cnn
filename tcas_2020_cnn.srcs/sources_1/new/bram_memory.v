`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UMBC-EEHPC
// Engineer: Nitheesh Manjunath 
// 
// Create Date: 05/05/2020 12:21:31 PM
// Design Name: 
// Module Name: bram_memory
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module bram_memory#(
    dw = 32,
    aw = 8,
    memFile = ""
    )(
    input wire clk,
    input wire [dw-1:0] data_in,
    input wire [aw-1:0] addr,
    input wire wr,
    input wire rd,
    output reg [dw-1:0] data_out
    );
    
    localparam RAM_DEPTH = 1 << aw;
    
   //(* ram_style =  "block" *) 
   reg  [dw-1:0] r_2p [RAM_DEPTH-1:0] ;
    
    initial begin
        $readmemh(memFile, r_2p); //to initialize the memories with the weight values
    end
    
    always @(posedge clk) begin
        if(wr) begin
            r_2p[addr] <= data_in;
        end
    end
    
    always @(posedge clk) begin
        if(rd) begin
            data_out <= r_2p[addr];
        end
        else begin
            data_out <= {dw{1'b0}};
        end
    end
endmodule
