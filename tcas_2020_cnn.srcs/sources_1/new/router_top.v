`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UMBC-EEHPC
// Engineer: Nitheesh Manjunath
// 
// Create Date: 05/04/2020 09:10:45 PM
// Design Name: 
// Module Name: router_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
//`define PE_4

module router_top
    #(
    dw = 8,
    PE = 4,
    log2PE = $clog2(PE)
    )(
    clk, en, r_in, in, ready, out
    );

input clk, en;
input [log2PE-1:0] r_in;
input [PE*dw-1:0] in;

output ready;
output [PE*dw-1:0] out;

wire ready_ip, ready_rtr, ready_op;

wire [PE*dw-1:0] ipRouterOut0, ipRouterOut1;
wire [PE*dw-1:0] lastrouterOut0, lastrouterOut1;

wire [dw-1:0] in00, in01, in02, in03;
wire [dw-1:0] in10, in11, in12, in13;

wire [dw-1:0] out00, out01, out02, out03;
wire [dw-1:0] out10, out11, out12, out13;

`ifdef PE_4

input_router #(
    .dw(dw),
    .PE(PE)
    )input_router_inst(
    .r_in(r_in[0]), 
    .in(in),
    .en(en),
    .ready(ready_ip), 
    .out0({in00, in01, in02, in03}),
    .out1({in10, in11, in12, in13})
    );

router
    #(
    .dw(dw),
    .PE(PE)
    )router_inst(
    .clk(clk), 
    .r_in(r_in),
    .en(ready_ip),
    .ready(ready_rtr),
    .in00(in00), .in01(in01), .in02(in02), .in03(in03), 
    .in10(in10), .in11(in11), .in12(in12), .in13(in13), 
    .out00(out00), .out01(out01), .out02(out02), .out03(out03), 
    .out10(out10), .out11(out11), .out12(out12), .out13(out13)
    );

output_router
    #(
    .dw(dw),
    .PE(PE)
    )output_router_inst(
    .r_in(r_in[log2PE-1]),
    .en(ready_rtr),
    .ready(ready_op),
    .in0({out00, out01, out02, out03}), 
    .in1({out10, out11, out12, out13}), 
    .out(out)
    );
    
assign ready = ready_op;

`else 
    reg ready_1,ready_2;
    reg [PE*dw-1:0] out_1, out_2;
    assign out = out_2;
    assign ready = ready_2;
    always@(posedge clk) begin
        ready_1 <= en;
        ready_2 <= ready_1;
        out_1 <= in;
        out_2 <= out_1;
    end
`endif



endmodule
