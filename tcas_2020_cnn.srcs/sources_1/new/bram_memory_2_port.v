`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/09/2020 08:19:23 PM
// Design Name: 
// Module Name: bram_memory_2_port
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module bram_memory_2_port#(
             dw = 32,
             aw = 8,
             memFile = ""
            )(
            clk, data_in, readAddr, rd, writeAddr, wr, data_out
            );
input wire clk;
input wire [dw-1:0] data_in;
input wire [aw-1:0] readAddr;
input wire rd;
input wire [aw-1:0] writeAddr;
input wire wr;
output reg [dw-1:0] data_out;
    
localparam RAM_DEPTH = 1 << aw;

reg  [dw-1:0] r_2p [RAM_DEPTH-1:0] ;
integer i;
initial begin
    $readmemb(memFile, r_2p); //to initialize the memories with the weight values
end

always @(posedge clk) begin
    if(wr) begin
        r_2p[writeAddr] <= data_in;
    end
end

always @(posedge clk) begin
    if(rd) begin
        data_out <= r_2p[readAddr];
    end
end
endmodule
