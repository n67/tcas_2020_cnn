`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UMBC-EEHPC
// Engineer: Nitheesh Manjunath 
// 
// Create Date: 05/05/2020 03:53:50 PM
// Design Name: 
// Module Name: CSCConv2D
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// f = filterNum/Row
//////////////////////////////////////////////////////////////////////////////////
//`define SIM_ONLY //uncomment this only during simulation

module CSCConv2D#(
    dw = 8,
    awFM = 8,
    awWM = 8,
    PE = 4,
    cw = 16,
    fw = 16,
    strideW = 4,
    cscw = 11, //This is for Ni, s, f and their counters
    sameSize = 1
    )(
    clk, rst_n, startConv, filterCol, filterRow, imageCol, imageRow, stride, f, s, No,
    rtrSignal, featureMapAddress, weightAddress, newMac, endMac, ConvDone, isConvRunning, firstConv, nextRow, nextCol
    );
 
 localparam log2PE = $clog2(PE);   
    
input clk;
input rst_n;
input startConv;
input [fw-1:0] filterCol;
input [fw-1:0] filterRow;
input [fw-1:0] imageCol;
input [fw-1:0] imageRow;
input [strideW-1:0] stride;
input [cscw-1:0] f;
input [cscw-1:0] s;
input [cscw-1:0] No;

output [$clog2(PE)-1:0] rtrSignal;
output  [PE*awFM-1:0] featureMapAddress;
output  [awWM-1:0] weightAddress;
output reg newMac;                                                //new row signal simlar to DenseLayer                             
output reg endMac;                                           //Intentionally Made it Combi, Once I pipeline the FM and WM addresses I'll make it Seq
output reg ConvDone;                                               //conv done Can be used to start unloading the data from the output memory to feature map mem
output reg isConvRunning;
output reg firstConv;
output reg nextRow;
output reg nextCol;


localparam [2:0] state_0 = 3'b000,
                 state_1 = 3'b001,
                 state_2 = 3'b010,
                 state_3 = 3'b011,
                 state_4 = 3'b100;
                 
(*keep = "true"*)reg [2:0] currentState;
(*keep = "true"*)reg startConv_3, startConv_2, startConv_1;

(*keep = "true"*)reg [cw-1:0] imageColCnt, imageRowCnt, denseCnt;
(*keep = "true"*)reg [cw-1:0] filterColCnt, filterRowCnt;
(*keep = "true"*)reg [cscw-1:0] cntNo;
(*keep = "true"*)reg [cscw-1:0] Ni;
(*keep = "true"*)reg resetFilterColCnt, resetFilterRowCnt, resetImageColCnt, resetImageRowCnt, rtrEn, enDense, nextCol_1, newMac_1; //nextCol_2,
(*keep = "true"*)reg [cscw-1:0] cntF ;//cntNi_0, cntNi_1, cntNi_2, cntNi_3;                                 //Must Change this according to the PE's  ***** cntNi_0 is the main Ni counter
//(*keep = "true"*)reg [cscw-1:0] cntNi_i0, cntNi_i1, cntNi_i2, cntNi_i3;
//(*keep = "true"*)reg [cscw-1:0] memAcc_0, memAcc_1, memAcc_2, memAcc_3; 
(*keep = "true"*)reg [awFM-1:0] singleImageFMDepth, singleFilterWMDepth;                           //imageRow*imageCol stored in FM, precalculating for the benifit for performance
(*keep = "true"*)reg [4-1:0] ColDiff;
(*keep = "true"*)reg [PE-1:0] cycRect;
(*keep = "true"*)reg [awWM-1:0] lastweightAddress;
(*keep = "true"*)reg [PE*cscw-1:0] cntNi, cntNi_i, memAcc;
(*keep = "true"*)reg [fw-1:0] filColxRow;
(*keep = "true"*)reg [awFM-1:0] fmAddress_1, fmAddress_2;
(*keep = "true"*)reg [awWM-1:0] weightAddr_2, weightAddr_1;

wire rtrReady, endMac_1;
wire [PE*cscw-1:0] xcntNi;//_0, xcntNi_1, xcntNi_2, xcntNi_3;
wire [awFM-1:0] fmAddress;
//wire [awWM-1:0] wmAddress;


router_top
    #(
    .dw(cscw),
    .PE(PE),
    .log2PE(log2PE)
    )offsetRouter(
    .clk(clk), 
    .en(rtrEn),
    .ready(rtrReady),
    .r_in({cntNi[0],cntNi[1]}),  //{cntNi_0[0],cntNi_0[1]}
    .in(cntNi), //{cntNi[cscw-1:0], cntNi[2*cscw-1:cscw], cntNi[3*cscw-1:2*cscw], cntNi[4*cscw-1:3*cscw]}
    .out(xcntNi) //{xcntNi[cscw-1:0], xcntNi[2*cscw-1:cscw], xcntNi[3*cscw-1:2*cscw], xcntNi[4*cscw-1:3*cscw]}
    );
    
    
always@(posedge clk or negedge rst_n)begin
    if(!rst_n) begin
        currentState <= state_0;
        Ni <= 0;
        isConvRunning <= 0;
        rtrEn <= 0;
//        memAcc_0 <= 0;
//        memAcc_1 <= 0;
//        memAcc_2 <= 0;
//        memAcc_3 <= 0;
        singleImageFMDepth <= 0;
        singleFilterWMDepth <= 0;
        cntF <= 0;
        cntNo <= 0;
        ConvDone <= 0;
        ColDiff <= 0;
        firstConv <= 0;
        nextRow <= 0;
        nextCol <= 0;
        enDense <= 0;
        lastweightAddress <= 0;
        filColxRow <= 0;
//        startConv_local <= 0;
    end
    else begin
        case(currentState) 
            state_0 : begin
                isConvRunning <= 0;
                cntF <= 0;
                cntNo <= 0;
                filColxRow <= 0;
//                memAcc_0 <= 0;
//                memAcc_1 <= 0;
//                memAcc_2 <= 0;
//                memAcc_3 <= 0;
                ConvDone <= 0;
                firstConv <= 1;
                lastweightAddress <= lastweightAddress;
//                startConv_local <= 0;
                if(startConv) begin
                    currentState <= state_1;
                    Ni <= f*s;
                    rtrEn <= 1;
                    singleImageFMDepth <= imageCol*imageRow;
                    singleFilterWMDepth <= filterCol*filterRow;
                    nextRow <= 1;
                    nextCol <= 1;
                    if(filterCol < 3) begin
                        ColDiff <= 1;
                    end
                    else begin
                        ColDiff <= 2;
                    end
                    
                    if(filterCol == 1 && filterRow == 1) begin
                        enDense <= 1;
                    end
                    else begin
                        enDense <= 0;
                    end
                end
                else begin
                    currentState <= state_0;
                    Ni <= Ni;
                    rtrEn <= 0;
                    ColDiff <= 0;
                    nextRow <= 0;
                    nextCol <= 0;
                    enDense <= 0;
                end
            end
            
            state_1 : begin
                nextCol <= 0;
                nextRow <= 0;
                isConvRunning <= 1;
                if(rtrReady) begin
//                    memAcc_0 <= xcntNi_0 >> log2PE;
//                    memAcc_1 <= xcntNi_1 >> log2PE;
//                    memAcc_2 <= xcntNi_2 >> log2PE;
//                    memAcc_3 <= xcntNi_3 >> log2PE;
                    currentState <= state_2;
                    rtrEn <= 0;
                    cntF <= cntF + 1;
//                    startConv_local <= 1;
                end
                else begin
                    currentState <= state_1;
                    rtrEn <= 1;
                end
            end
            
            state_2 : begin
//                startConv_local <= 0;
                isConvRunning <= 1;
                if(resetImageRowCnt) begin
                    currentState <= state_2;
                end 
                else begin
                    currentState <= state_3;
                end
            end
            
            state_3 : begin
                if(cntF < f) begin
                    currentState <= state_1;
                    rtrEn <= 1;
                    firstConv <= 0;
                    nextCol <= 1;
                end
                else begin
                    currentState <= state_4;
                    cntNo <= cntNo + PE;
                    firstConv <= 1;
                    filColxRow <= filterCol * filterRow;
                end
            end
            
            state_4 : begin
                cntF <= 0;
                lastweightAddress <= weightAddress + filColxRow;
                if(cntNo < No) begin
                    currentState <= state_1;
                    rtrEn <= 1;
                    nextRow <= 1;
                    nextCol <= 1;
                end
                else begin
                    cntNo <= 0;
                    currentState <= state_0;
                    ConvDone <= 1;
                end
            end
            
        endcase
    
    end
end

always@(posedge clk or negedge rst_n) begin
    if(!rst_n) begin
        filterColCnt <= 0;
        filterRowCnt <= 0;
//        cntNi_0 <= 0;
//        cntNi_1 <= 1; 
//        cntNi_2 <= 2;
//        cntNi_3 <= 3; 
//        cntNi_i0 <= 0;
//        cntNi_i1 <= 1; 
//        cntNi_i2 <= 2;
//        cntNi_i3 <= 3;
    end
    else begin
        case(currentState) 
            state_2 : begin;
                if(!resetFilterColCnt) begin
                    filterRowCnt <= filterRowCnt * resetFilterRowCnt;
                    filterColCnt <= (filterColCnt + 1);
                end
                else begin
                    filterColCnt <= 0;
                    filterRowCnt <= (filterRowCnt + 1) * resetFilterRowCnt;
                end
            end
            
//            state_3 : begin
//                if(cntF < f) begin
////                    cntNi_0 <= cntNi_0*cycRect[0] + s;
////                    cntNi_1 <= cntNi_1*cycRect[1] + s; 
////                    cntNi_2 <= cntNi_2*cycRect[2] + s;
////                    cntNi_3 <= cntNi_3*cycRect[3] + s; 
//                end
//                else begin
////                    cntNi_0 <= cntNi_i0;
////                    cntNi_1 <= cntNi_i1; 
////                    cntNi_2 <= cntNi_i2;
////                    cntNi_3 <= cntNi_i3; 
//                end
//            end
            
//            state_4 : begin
////                cntNi_0 <= cntNi_0 + PE;
////                cntNi_1 <= cntNi_1 + PE; 
////                cntNi_2 <= cntNi_2 + PE;
////                cntNi_3 <= cntNi_3 + PE;
                
////                cntNi_i0 <= cntNi_i0 + PE;
////                cntNi_i1 <= cntNi_i1 + PE; 
////                cntNi_i2 <= cntNi_i2 + PE;
////                cntNi_i3 <= cntNi_i3 + PE; 
//            end
        endcase
    end
end

genvar j;
generate
    for(j=0; j<PE; j=j+1) begin
        always@(posedge clk or negedge rst_n) begin
            if(!rst_n) begin
                cntNi[(j+1)*cscw-1:j*cscw] <= j;
                memAcc[(j+1)*cscw-1:j*cscw] <= 0; 
            end
            else begin
                case(currentState)
                    state_0 : begin
                        cntNi[(j+1)*cscw-1:j*cscw] <= j; 
                        cntNi_i[(j+1)*cscw-1:j*cscw] <= j;
                        memAcc[(j+1)*cscw-1:j*cscw] <= 0;
                    end
                    state_1 : begin
                        memAcc[(j+1)*cscw-1:j*cscw] <= xcntNi[(j+1)*cscw-1:j*cscw] >> log2PE;
                    end
                    state_3 : begin
                    if(cntF < f) begin
                        cntNi[(j+1)*cscw-1:j*cscw] <= (cntNi[(j+1)*cscw-1:j*cscw] + s) * cycRect[j]; 
                    end
                    else begin
                        cntNi[(j+1)*cscw-1:j*cscw] <= cntNi_i[(j+1)*cscw-1:j*cscw];
                    end
                    end
                    
                    state_4 : begin
                        cntNi[(j+1)*cscw-1:j*cscw] <= cntNi[(j+1)*cscw-1:j*cscw] + PE;
                        cntNi_i[(j+1)*cscw-1:j*cscw] <= cntNi_i[(j+1)*cscw-1:j*cscw] + PE; 
                    end
                    
                    default : begin
                        cntNi[(j+1)*cscw-1:j*cscw] <= cntNi[(j+1)*cscw-1:j*cscw];
                        cntNi_i[(j+1)*cscw-1:j*cscw] <= cntNi_i[(j+1)*cscw-1:j*cscw];
                    end
               endcase
            end
        end
    end
endgenerate

genvar i;
generate
    for(i=0; i<PE; i=i+1) begin
        always@(posedge clk or negedge rst_n) begin
            if(!rst_n) begin
                cycRect[i] <= 1;
            end
            else begin
               if(currentState == state_2 && cntNi[(i+1)*cscw-1:i*cscw] >= f-1) begin
                    cycRect[i] <= 0;
               end
               else begin
                    cycRect[i] <= 1;
               end 
            end
        end
    end
endgenerate

always@(posedge clk or negedge rst_n) begin
    if(!rst_n) begin
        imageColCnt <= 0;
    end
    else begin 
        if(resetFilterColCnt && !resetFilterRowCnt) begin
            imageColCnt <= (imageColCnt + stride) * resetImageColCnt;
        end
        else begin
            imageColCnt <= imageColCnt * resetImageColCnt;
        end
    end
end

always@(posedge clk or negedge rst_n) begin
    if(!rst_n) begin
        imageRowCnt <= 0;
    end
    else begin
        if(resetFilterColCnt && !resetFilterRowCnt && !resetImageColCnt) begin
            imageRowCnt <= (imageRowCnt + stride) * resetImageRowCnt;
        end
        else begin
            imageRowCnt <= imageRowCnt * resetImageRowCnt ;
        end
    end
end


always@(*) begin
    if(!rst_n) begin
        resetFilterColCnt <= 0;
    end
    else begin
        if(filterColCnt == filterCol-1 && currentState == state_2) begin
            resetFilterColCnt <= 1;
        end
        else begin
            resetFilterColCnt <= 0;
        end
    end
end

always@(*) begin
    if(!rst_n) begin
        resetFilterRowCnt <= 0;
    end
    else begin
        if(filterRowCnt == filterRow-1 && filterColCnt == filterCol-1 && currentState == state_2) begin
            resetFilterRowCnt <= 0;
        end
        else begin
            resetFilterRowCnt <= 1;
        end
    end
end

always@(*) begin
    if(!rst_n) begin
        resetImageColCnt <= 0;
    end
    else begin
        if((imageColCnt >= (imageCol-filterCol*!sameSize)) && filterRowCnt == filterRow-1 && filterColCnt == filterCol-1 && currentState == state_2) begin 
            resetImageColCnt <= 0;
        end
        else begin
            resetImageColCnt <= 1;
        end
    end
end

always@(*) begin
    if(!rst_n) begin
        resetImageRowCnt <= 0;
    end
    else begin
        if((imageRowCnt >= (imageRow-filterRow*!sameSize)) && (imageColCnt >= (imageCol-filterCol*!sameSize)) && filterRowCnt == filterRow-1 && filterColCnt == filterCol-1 && currentState == state_2) begin
            resetImageRowCnt <= 0;
        end
        else begin
            resetImageRowCnt <= 1;
        end
    end
end


always@(posedge clk or negedge rst_n) begin
    if(!rst_n || currentState == state_0) begin
        denseCnt <= 0;
    end
    else begin
        if(enDense && (currentState == state_1 || currentState == state_2)) begin
            denseCnt <= denseCnt + (nextCol_1 & !startConv_2);
        end
        else begin
            denseCnt <= denseCnt;
        end
    end
end

always@(posedge clk) begin
    startConv_1 <= startConv;
    startConv_2 <= startConv_1;
//    startConv_3 <= startConv_2;
    newMac_1 <= isConvRunning & ((rtrReady & rtrEn) | !resetFilterRowCnt);
    newMac <= newMac_1;
    nextCol_1 <= nextCol;
    endMac <= endMac_1;
//    nextCol_2 <= nextCol_1;
end

always@(posedge clk or negedge rst_n) begin
    if(!rst_n) begin
        fmAddress_1 <= 0;
        fmAddress_2 <= 0;
        weightAddr_2 <= 0;
        weightAddr_1 <= 0;
    end
    else begin
        fmAddress_1 <= (imageRowCnt  +  filterRowCnt ) * imageCol;
        fmAddress_2 <= imageColCnt  + filterColCnt;
        weightAddr_2 <= filterRow * filterRowCnt + denseCnt + lastweightAddress;
        weightAddr_1 <= (cntF-1)*singleFilterWMDepth + filterColCnt;
    end
end

assign fmAddress = fmAddress_1 + fmAddress_2;
assign weightAddress = weightAddr_1 + weightAddr_2;

//assign fmAddress = (imageRowCnt  +  filterRowCnt ) * imageCol + imageColCnt  + filterColCnt;
//assign weightAddress = (cntF-1)*singleFilterWMDepth + filterColCnt +  filterRow * filterRowCnt + denseCnt + lastweightAddress;

genvar k;
generate
    for(k=0; k<PE; k=k+1) begin
        assign featureMapAddress[((PE-k)*awFM-1):(PE-(k+1))*awFM] = memAcc[(k+1)*cscw-1:k*cscw]*singleImageFMDepth + fmAddress;
    end
endgenerate

//assign featureMapAddress[((PE)*awFM-1):(PE-1)*awFM] = memAcc_0*singleImageFMDepth + fmAddress;
//assign featureMapAddress[((PE-1)*awFM-1):(PE-2)*awFM] = memAcc_1*singleImageFMDepth + fmAddress;
//assign featureMapAddress[((PE-2)*awFM-1):(PE-3)*awFM] = memAcc_2*singleImageFMDepth + fmAddress;
//assign featureMapAddress[((PE-3)*awFM-1):(PE-4)*awFM] = memAcc_3*singleImageFMDepth + fmAddress;

assign endMac_1 = isConvRunning & !resetFilterRowCnt;
assign rtrSignal = cntNi[$clog2(PE)-1:0];

//for simulation only
`ifdef SIM_ONLY
wire [awFM-1:0]featureMapAddress_3, featureMapAddress_2, featureMapAddress_1, featureMapAddress_0;

wire [cscw-1:0] cntNi_0, cntNi_1, cntNi_2, cntNi_3;                                 //Must Change this according to the PE's  ***** cntNi_0 is the main Ni counter
wire  [cscw-1:0] cntNi_i0, cntNi_i1, cntNi_i2, cntNi_i3;

assign featureMapAddress_0 = featureMapAddress[((PE)*awFM-1):(PE-1)*awFM];
assign featureMapAddress_1 = featureMapAddress[((PE-1)*awFM-1):(PE-2)*awFM];
assign featureMapAddress_2 = featureMapAddress[((PE-2)*awFM-1):(PE-3)*awFM];
assign featureMapAddress_3 = featureMapAddress[((PE-3)*awFM-1):(PE-4)*awFM];

assign cntNi_0 = cntNi[cscw-1:0] ;
assign cntNi_1 = cntNi[2*cscw-1:cscw];
assign cntNi_2 = cntNi[3*cscw-1:2*cscw];
assign cntNi_3 = cntNi[4*cscw-1:3*cscw];

assign cntNi_i0 = cntNi_i[cscw-1:0] ;
assign cntNi_i1 = cntNi_i[2*cscw-1:cscw];
assign cntNi_i2 = cntNi_i[3*cscw-1:2*cscw];
assign cntNi_i3 = cntNi_i[4*cscw-1:3*cscw];

`endif

endmodule
