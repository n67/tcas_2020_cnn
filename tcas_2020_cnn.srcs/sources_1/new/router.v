`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UMBC-EEHPC
// Engineer: Nitheesh Manjunath 
// 
// Create Date: 05/04/2020 08:09:30 PM
// Design Name: 
// Module Name: router
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: Made it Sync so that we have max frequency. But needs to take care of this when we increse the
//                      PE the pipeline of others also need to be matched with this 
//////////////////////////////////////////////////////////////////////////////////


module router
    #(
    dw = 8,
    PE = 4
    )(
    clk, r_in, en, ready,
    in00, in01, in02, in03, in10, in11, in12, in13, 
    out00, out01, out02, out03, out10, out11, out12, out13
    );

input clk, en;
input [1:0] r_in;
input [dw-1:0] in00, in01, in02, in03;
input [dw-1:0] in10, in11, in12, in13;

output reg ready;
output reg [dw-1:0] out00, out01, out02, out03;
output reg [dw-1:0] out10, out11, out12, out13;

always@(posedge clk) begin
    case({en,r_in})
        3'b100 : begin
            {out00, out01, out02, out03} <= {in00, in01, in02, in03};
            {out10, out11, out12, out13} <= {PE*dw{1'bz}};
            ready <= 1;
        end
        3'b101 : begin
            {out00, out01, out02, out03} <= {in12, in13, in10, in11};
            {out10, out11, out12, out13} <= {PE*dw{1'bz}};
            ready <= 1;
        end
        3'b110 : begin
            {out00, out01, out02, out03} <= {PE*dw{1'bz}};
            {out13, out10, out11, out12} <= {in00, in01, in02, in03};
            ready <= 1;
        end
        3'b111 : begin
            {out00, out01, out02, out03} <= {PE*dw{1'bz}};
            {out13, out10, out11, out12} <= {in12, in13, in10, in11};
            ready <= 1;
        end
        default : begin
            {out00, out01, out02, out03} <= {PE*dw{1'bz}};
            {out10, out11, out12, out13} <= {PE*dw{1'bz}};
            ready <= 0;
        end
    endcase
end

endmodule
