`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UMBC-EEHPC
// Engineer: Nitheesh Manjunath 
// 
// Create Date: 03/03/2020 04:07:13 PM
// Design Name: 
// Module Name: MAC
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MAC#(
    dw_i = 8,
    dw_o = 20
    )(
    clk, rst_n, newMac, A, B, P
    );
    
input clk;
input rst_n;

input newMac;
input signed [dw_i-1:0] A;
input signed [dw_i-1:0] B;
output signed [dw_o-1:0] P;
    
//reg signed [dw_o-1:0] partial_product;
reg signed [dw_o-1:0] partialSum;


//always@(posedge clk or negedge rst_n) begin
//    if(!rst_n) begin
//        partial_product <= 0;
//    end
//    else begin
//        partial_product <= A*B;
//    end 
//end

always@(posedge clk or negedge rst_n) begin
    if(!rst_n) begin
        partialSum <= 0;
    end
    else begin
        if(!newMac) begin
            partialSum <= partialSum + A*B;
        end
        else begin
            partialSum <= A*B;
        end
    end
end

assign P = partialSum;
   

endmodule
