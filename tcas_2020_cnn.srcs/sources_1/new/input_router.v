`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UMBC-EEHPC
// Engineer: Nitheesh Manjunath 
// 
// Create Date: 05/04/2020 05:47:40 PM
// Design Name: 
// Module Name: input_router
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: Fully Asyncronuos, IF critical path is in this module please change it to synchronous and non-blocking assignment
// 
//////////////////////////////////////////////////////////////////////////////////


module input_router
    #(
    dw = 8,
    PE = 4
    )(r_in, in, en, ready, out0, out1);


input r_in;
input [PE*dw-1:0] in;
input en;

output reg ready;
output reg [PE*dw-1:0] out0;
output reg [PE*dw-1:0] out1;
    
always@(*) begin
    case({en,r_in}) 
        2'b10 : begin
            out0 = in;
            out1 = {PE*dw{1'bz}};
            ready = 1;
        end
        2'b11 : begin
            out0 = {PE*dw{1'bz}};
            out1 = in;
            ready = 1;
        end
        default : begin
            ready = 0;
            out0 = {PE*dw{1'bz}};
            out1 = {PE*dw{1'bz}};
        end
    endcase
end
    
endmodule
