`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UMBC-EEHPC
// Engineer: Nitheesh Manjunath
// 
// Create Date: 05/07/2020 02:09:47 PM
// Design Name: 
// Module Name: top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
//`define SIMONLY;

/*
Config memory in Hexadecimal
MemLocation         Parameter               Example
    0.              Valid/NotValid          1/0
    1.              Image Column            E0
    2.              Image Row               E0
    3.              Filter Col              3
    4.              Filter Row              3
    5.              Stride                  2
    6.              f                       10
    7.              s                       1
    8.              No                      4    
*/

module top#(
    PE = 4,
    dw = 8,
    dw_o = 24,                                      //MAC Output width
    awFM = 15,                                      //awFM 2^15 locations/PE | should satisfy awFM < awOM
    awWM = 18,
    awOM = 15,
    awCM = 8,
    awMax = awWM,                                   //max of awFM, awWM, awCM, awOM
    numConfig = 9,
    cscw = 19,                                      //This is for Ni, s, f and their counters
    cw = 19,                                        //Counter Width
    fw = 19,                                        //Filter Width
    dw_i = fw,                                      //max(dw, fw)
    strideW = 4,
    configFile = "config.mem",
    memFile = "placeholder.mem"
    )(
    clk_i, rst_n_i, start_i, data_i, valid_i, clr_addr_i, nextMemory_wr_i, memorySelect_i, req_i, data_o, valid_o, done_o
    );
input clk_i;
input rst_n_i;
input start_i;
input [dw_i-1:0] data_i;
input valid_i;
input clr_addr_i;
input nextMemory_wr_i;
input [1:0] memorySelect_i;

input req_i;

output  [dw-1:0] data_o;
output  valid_o;
output reg done_o;


localparam state_0 = 0,
           state_1 = 1,
           state_2 = 2,
           state_3 = 3,
           state_4 = 4,
           state_5 = 5,
           state_6 = 6,
           state_7 = 7;
           

//Registers
reg [2:0] currentState;
reg read_CM, read_CM_1;
reg [fw*8-1:0] shiftRegConfig;
reg [4-1:0] configCnt;                           //Number of Config parameter are 8
reg [awCM-1:0] CMReadAddr, CMWriteAddr;
reg startConv, newMac_1, newMac_2, newMac_3, endMac_1, endMac_2, endMac_3, rtrEn, writeFM_2, readFM2o, valid, isConvRunning_1;
reg firstConv_1, firstConv_2, firstConv_3, nextRow_1, nextCol_1, nextRow_2, nextCol_2, nextRow_3, nextCol_3;
reg [PE*dw-1:0] WeightDataout_1, activatedOutput_1;
reg [awMax-1:0] modelLoadingAddress;
reg [awFM-1:0] OM2FMAddress_1, maxFMAddress, FM2oAddress;
reg [$clog2(PE) : 0] rdEn;
reg [$clog2(PE)-1: 0]rdEn_1;
reg [PE*awFM-1:0] featureMapAddress_1;
reg [awWM-1:0] weightAddress_1;
//Wires
wire [fw-1:0] dataOutCM;
wire [awCM-1:0] CMAddr;
wire [PE*awFM-1:0] featureMapAddress, addrFMRead;
wire [awWM-1:0] weightAddress;
wire newMac, endMac, ConvDone, isConvRunning;
wire [PE*dw-1:0] FeatureMapDataout, WeightDataout, FeatureMapDataoutRtr, activatedOutput;
wire [$clog2(PE)-1:0] r_in;
wire rtrReady, firstConv;
wire nextRow, nextCol, doneLoading, writeFM_1;
wire [PE*dw_o-1:0] MACDataout, activationOutput;
wire [awFM-1:0] OM2FMAddress;
wire [4-1:0] wrEnBit;
wire [PE*dw-1:0] inputFMdata;
wire [awFM-1:0] addrFM;

//Instantiaition
n_bit_decoder#(
    .PE(4),
    .log2_PE($clog2(4))
    )decoder(
    .en(valid_i),
    .i(memorySelect_i),
    .out(wrEnBit)
    );
    
bram_memory#(
    .dw(fw),
    .aw(awCM), 
    .memFile(configFile)
    )CM(
    .clk(clk_i),
    .data_in(data_i[cscw-1:0]),
    .addr(CMAddr),
    .wr(wrEnBit[1]),
    .rd(read_CM),
    .data_out(dataOutCM)
    );

CSCConv2D#(
    .dw(dw),
    .awFM(awFM),
    .awWM(awWM),
    .PE(PE),
    .cw(cw),
    .fw(fw),
    .strideW(strideW),
    .cscw(cscw)
    )CSCAddressDataControl(
    .clk(clk_i), 
    .rst_n(rst_n_i), 
    .startConv(startConv),
    .imageCol(shiftRegConfig[fw*8-1:fw*7]), 
    .imageRow(shiftRegConfig[fw*7-1:fw*6]),  
    .filterCol(shiftRegConfig[fw*6-1:fw*5]), 
    .filterRow(shiftRegConfig[fw*5-1:fw*4]), 
    .stride(shiftRegConfig[fw*4-1-(fw-strideW):fw*3]), 
    .f(shiftRegConfig[fw*3-1-(fw-cscw):fw*2]), 
    .s(shiftRegConfig[fw*2-1-(fw-cscw):fw]), 
    .No(shiftRegConfig[fw-1-(fw-cscw):0]),
    .rtrSignal(r_in),
    .featureMapAddress(featureMapAddress), 
    .weightAddress(weightAddress), 
    .newMac(newMac), 
    .endMac(endMac), 
    .ConvDone(ConvDone), 
    .isConvRunning(isConvRunning),
    .firstConv(firstConv),
    .nextRow(nextRow),
    .nextCol(nextCol)
    );

FM_wrapper#(
    .dw(dw),
    .aw(awFM),
    .PE(PE),
    .memFile(memFile)
    )FeatureMapMemory(
    .clk(clk_i), 
    .rst_n(rst_n_i), 
    .Input(inputFMdata), 
    .writeAddress(addrFM), 
    .nextMemoryWrite(nextMemory_wr_i), 
    .write(wrEnBit[2]), 
    .writeAll(writeFM_2), 
    .read(isConvRunning_1), 
    .readAddress(addrFMRead),
    .reado(readFM2o), 
    .rdEn(rdEn[$clog2(PE)-1:0]),
    .Output(FeatureMapDataout)
    );

WM_wrapper#(
    .dw(dw),
    .aw(awWM),
    .PE(PE),
    .memFile(memFile)
    )WeightMemory(
    .clk(clk_i), 
    .rst_n(rst_n_i), 
    .Input(data_i[dw-1:0]), 
    .read(isConvRunning_1), 
    .nextMemoryWrite(nextMemory_wr_i), 
    .write(wrEnBit[3]), 
    .writeAddress(modelLoadingAddress[awWM-1:0]), 
    .readAddress(weightAddress_1), 
    .Output(WeightDataout)
    );
    
    
router_top
    #(
    .dw(dw),
    .PE(PE),
    .log2PE($clog2(PE))
    )offsetRouter(
    .clk(clk_i), 
    .en(rtrEn),
    .ready(rtrReady),
    .r_in({r_in[0],r_in[1]}), 
    .in(FeatureMapDataout),
        //{FeatureMapDataout[((PE-3)*dw-1):(PE-4)*dw], FeatureMapDataout[((PE-2)*dw-1):(PE-3)*dw], 
        // FeatureMapDataout[((PE-1)*dw-1):(PE-2)*dw], FeatureMapDataout[((PE)*dw-1):(PE-1)*dw]}
    .out({FeatureMapDataoutRtr})  //Verify if this is in the right order
    );
    

genvar i;
generate 
    for(i=0; i<PE; i=i+1) begin
        MAC#(
        .dw_i(dw),
        .dw_o(dw_o)
        )MU(
        .clk(clk_i),
        .rst_n(rst_n_i),
        .newMac(newMac_3),
        .A(FeatureMapDataoutRtr[dw*(i+1)-1:dw*i]),
        .B(WeightDataout_1[dw*(i+1)-1:dw*i]),
        .P(MACDataout[dw_o*(i+1)-1:dw_o*i]));
    end
endgenerate

OM_wrapper#(
    .dw(dw_o),
    .aw(awOM),
    .awFM(awFM),
    .PE(PE),
    .memFile()
    )OutputMemory(
    .clk(clk_i), 
    .rst_n(rst_n_i), 
    .Input(MACDataout), 
    .nextRow(nextRow_3), 
    .nextCol(nextCol_3), 
    .write(endMac_3), 
    .convDone(ConvDone),
    .firstConv(firstConv_3),
    .Output_o(activationOutput),
    .OM2FMAddress_o(OM2FMAddress),
    .isLoading(writeFM_1),
    .doneLoading(doneLoading)
    );



//ReLu
genvar j;
generate
    for(j=0; j<PE; j=j+1) begin
        assign activatedOutput[dw*(j+1)-1:dw*j] = writeFM_1 ? activationOutput[dw_o*(j+1)-1] ? 0 : activationOutput[dw_o*j+dw-1:dw_o*j] : 0;
    end
endgenerate

//assign activatedOutput[7:0] = writeFM_1 ? activationOutput[dw_o-1] ? 0 : activationOutput[7:0] : 0;
//assign activatedOutput[15:8] = writeFM_1 ? activationOutput[dw_o*2-1] ? 0 : activationOutput[dw_o+7:dw_o] : 0;
//assign activatedOutput[23:16] = writeFM_1 ? activationOutput[dw_o*3-1] ? 0 : activationOutput[2*dw_o+7:2*dw_o] : 0;
//assign activatedOutput[31:24] = writeFM_1 ? activationOutput[dw_o*4-1] ? 0 : activationOutput[3*dw_o+7:3*dw_o] : 0;


always@(posedge clk_i) begin : pipeline
    read_CM_1 <= read_CM;
    newMac_1 <= newMac;
    newMac_2 <= newMac_1;
    newMac_3 <= newMac_2;
    firstConv_1 <= firstConv;
    firstConv_2 <= firstConv_1;
    firstConv_3 <= firstConv_2;
    endMac_1 <= endMac;
    endMac_2 <= endMac_1;
    endMac_3 <= endMac_2;
    WeightDataout_1 <= WeightDataout;
    nextRow_1 <= nextRow;
    nextCol_1 <= nextCol;
    nextRow_2 <= nextRow_1;
    nextCol_2 <= nextCol_1;
    nextRow_3 <= nextRow_2;
    nextCol_3 <= nextCol_2;
    activatedOutput_1 <= activatedOutput;
    OM2FMAddress_1 <= OM2FMAddress;
    writeFM_2 <= writeFM_1;
    valid <= readFM2o;
    rdEn_1 <= rdEn[$clog2(PE)-1:0];
    featureMapAddress_1 <= featureMapAddress;
    isConvRunning_1 <= isConvRunning;
    weightAddress_1 <= weightAddress;
end : pipeline


always@(posedge clk_i or negedge rst_n_i) begin 
    if(rst_n_i) begin
        modelLoadingAddress <= 0;
    end
    else begin
        casez({clr_addr_i, valid_i})
            2'b1? : modelLoadingAddress <= 0;
            2'b01 : modelLoadingAddress <= modelLoadingAddress + 1;
            default :  modelLoadingAddress <= modelLoadingAddress;
        endcase
    end
end

always@(posedge clk_i or negedge rst_n_i) begin
    if(!rst_n_i || done_o) begin
        CMWriteAddr <= 0;
    end
    else begin
        if(wrEnBit[1]) begin
            CMWriteAddr <= CMWriteAddr + 1;
        end
        else begin
            CMWriteAddr <= CMWriteAddr;
        end
    end
end

//FiniteStateMachine
always@(posedge clk_i or negedge rst_n_i) begin
    if(!rst_n_i) begin
        currentState <= state_0;
        read_CM <= 0;
        startConv <= 0;
//        readWMFM <= 0;
        rtrEn <= 0;
        done_o <= 1;
        readFM2o <= 0; 
    end
    else begin
        case(currentState)
            state_0 : begin
                startConv <= 0;
//                readWMFM <= 0;
                rtrEn <= 0;
                if(start_i) begin
                    currentState <= state_1;
                    read_CM <= 1;
                    done_o <= 0;
                    readFM2o <= 0;
                end
                else if(req_i) begin
                    currentState <= state_7;
                    readFM2o <= 1;
                end
                else begin
                    currentState <= state_0;
                    read_CM <= 0;
                    done_o <= 1;
                    readFM2o <= 0;
                end
            end
            
            state_1 : begin
                currentState <= state_2;
            end
            
            state_2 : begin
                if(dataOutCM == 1) begin
                    currentState <= state_3;
                end
                else begin
                    currentState <= state_0;
                end
            end
            
            state_3 : begin
                if(configCnt == 0) begin
                    currentState <= state_4;
                    read_CM <= 0;
                end
                else begin
                    currentState <= state_3;
                    read_CM <= 1;
                end
            end
            state_4 : begin
                read_CM <= 0;
                startConv <= 1;
//                readWMFM <= 1;
                rtrEn <= 1;
                currentState <= state_5;
            end
            state_5 : begin
                startConv <= 0;
//                readWMFM <= 1;
                if(ConvDone) begin
                    currentState <= state_6;
                end
                else begin
                    currentState <= state_5;
                end
            end
            state_6 : begin
                if(doneLoading) begin
                    currentState <= state_1;
                    read_CM <= 1;
                end
                else begin
                    currentState <= state_6;
                    read_CM <= 0;
                end
            end
            
            state_7 : begin
                if(rdEn >= PE) begin
                    currentState <= state_0;
                    readFM2o <= 0; 
                end
                else begin
                    currentState <= state_7;
                    readFM2o <= 1;
                end
            end
            default : begin
                currentState <= state_0;
            end
        endcase
    end
end


//Read Address for Configuration Memory
always@(posedge clk_i or negedge rst_n_i) begin
    if(!rst_n_i) begin
        configCnt <= numConfig - 1;
        CMReadAddr <= 0;
    end
    else begin
        if(read_CM) begin
            configCnt <= configCnt - 1;
            CMReadAddr <= CMReadAddr + 1;
        end
        else begin
            configCnt <= numConfig-1;
            CMReadAddr <= CMReadAddr;

        end 
    end
end

always@(posedge clk_i or negedge rst_n_i) begin
    if(!rst_n_i) begin
        shiftRegConfig <= 0;
    end
    else begin
        if(read_CM_1) begin
            shiftRegConfig <= {shiftRegConfig[fw*7-1:0],dataOutCM};
        end
        else begin
            shiftRegConfig <= shiftRegConfig;
        end
    end
end

always@(posedge clk_i or negedge rst_n_i) begin
    if(!rst_n_i) begin
        maxFMAddress <= 0; 
    end
    else begin
        if(writeFM_1) begin
            maxFMAddress <= OM2FMAddress;
        end
        else begin
            maxFMAddress <= maxFMAddress;
        end
    end
end

always@(posedge clk_i or negedge rst_n_i) begin
    if(!rst_n_i || currentState == state_0) begin
        FM2oAddress <= 0;
        rdEn <= 0;
    end
    else begin
        if(readFM2o && FM2oAddress < maxFMAddress) begin
            FM2oAddress <= FM2oAddress + 1;
            rdEn <= rdEn;
        end
        else begin
            FM2oAddress <= 0;
            rdEn <= rdEn + 1;
        end
    end
end

//always@(posedge clk_i or negedge rst_n_i) begin
//    if(!rst_n_i) begin
//        data_o <= 0;
//        valid_o <= 0;
//    end
//    else begin
//        data_o <= FeatureMapDataout[rdEn[$clog2(PE)-1:0]*dw +: dw];
//        if(rdEn < PE) begin
//            valid_o <= valid;
//        end
//    end
//end 




assign CMAddr = (memorySelect_i == 1) ? CMWriteAddr : CMReadAddr;
assign inputFMdata = currentState == state_0 ? {PE{data_i}} : activatedOutput_1;
assign addrFM = currentState == state_0 ? modelLoadingAddress[awFM-1:0] : OM2FMAddress_1;
assign addrFMRead = currentState == state_7 ?  FM2oAddress : featureMapAddress_1;


assign data_o = FeatureMapDataout[dw*rdEn_1 +: dw] ;
assign valid_o =  valid ;





//Simulation only

`ifdef SIMONLY
    wire [fw-1:0] imageCol;
    assign imageCol = shiftRegConfig[fw*8-1:fw*7];
`endif

endmodule
