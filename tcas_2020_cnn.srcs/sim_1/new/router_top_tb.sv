`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/04/2020 09:50:48 PM
// Design Name: 
// Module Name: router_top_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module router_top_tb    
    #(
    dw = 8,
    PE = 4
    );
localparam log2PE = $clog2(PE);
integer period = 10, i;
localparam num_tv = 10;

reg clk;
reg [log2PE-1:0] r_in;

reg [PE*dw-1:0] in;
wire [PE*dw-1:0] out;

router_top
    #(
    .dw(dw),
    .PE(PE)
    )uut(
    .clk(clk),
    .r_in(r_in),
    .in(in),
    .out(out)
    );

initial begin
    clk = 0;
    forever #(period/2) clk = ~clk;
end

initial begin
    static integer x = $random;
    
    for(i=0; i<4; i++) begin
        in = x;
        r_in = i;
        #(3*period);
    end
    
    $stop;
end


initial begin
  $dumpfile("router_top.vcd");
  $dumpvars;
end
endmodule
