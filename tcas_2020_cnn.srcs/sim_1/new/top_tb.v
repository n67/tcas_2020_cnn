`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/20/2020 09:00:09 PM
// Design Name: 
// Module Name: top_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top_tb(

    );

reg clk_i, rst_n_i, start_i, valid_i, clr_addr_i, nextMemory_wr_i, req_i;
reg [18-1:0] data_i;
reg [1:0] memorySelect_i;
wire [16-1:0] data_o;
wire valid_o, done_o;

top uut(clk_i, rst_n_i, start_i, data_i, valid_i, clr_addr_i, nextMemory_wr_i, memorySelect_i, req_i, data_o, valid_o, done_o);

integer period = 10, i ,j;

initial begin
    clk_i = 0;
    forever #(period/2) clk_i = ~clk_i;
end

initial begin
    rst_n_i = 0;
    start_i = 0;
    memorySelect_i = 0;
    data_i = 0;
    valid_i = 0;clr_addr_i = 0;
    req_i = 0;
    
    #(5*period);
    rst_n_i = 1;
    
    #(5*period);
    
    start_i = 1;
    #(period);
    
    start_i = 0;
    #(period);
    
    while(!done_o) #(period);
    
    #(10*period);
    
    req_i = 1;
    #(period);
    
    req_i = 0;
    #(450*4*period);
    
    
    #(10*period);
    $finish();
    
end
endmodule
