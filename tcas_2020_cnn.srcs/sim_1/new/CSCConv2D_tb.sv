`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/05/2020 05:26:04 PM
// Design Name: 
// Module Name: CSCConv2D_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module CSCConv2D_tb#(
    dw = 8,
    awFM = 10,
    awWM = 10,
    PE = 4,
    cw = 16,
    fw = 16
    );
    
reg clk;
reg rst_n;
reg startConv;
reg [fw-1:0] filterCol;
reg [fw-1:0] filterRow;
reg [fw-1:0] imageCol;
reg [fw-1:0] imageRow;
reg [4-1:0] stride;
reg [fw-1:0] f;
reg [fw-1:0] s;
reg [fw-1:0] No;

wire [PE*awFM-1:0] featureMapAddress;
wire [awWM-1:0] weightAddress;
wire newMac;                                                                            
wire endMac;                                          
wire ConvDone;                                               
wire isConvRunning;
    
integer period = 10, i, j;
    
CSCConv2D#(
    dw,
    awFM,
    awWM,
    PE,
    cw,
    fw
    )uut(
    .clk(clk), 
    .rst_n(rst_n), 
    .startConv(startConv), 
    .filterCol(filterCol), 
    .filterRow(filterRow), 
    .imageCol(imageCol), 
    .imageRow(imageRow), 
    .stride(stride), 
    .f(f), 
    .s(s), 
    .No(No),
    .featureMapAddress(featureMapAddress), 
    .weightAddress(weightAddress), 
    .newMac(newMac), 
    .endMac(endMac), 
    .ConvDone(ConvDone), 
    .isConvRunning(isConvRunning)
    );

initial begin
    clk = $random;
    forever #(period/2) clk = ~clk;
end

initial begin
    rst_n = 0;
    startConv = 0;
    filterCol = 0;
    filterRow = 0;
    imageCol = 0;
    imageRow = 0;
    stride = 0;
    f = 0;
    s = 0;
    No = 0;
    #(period);
    
    rst_n = 1;
    #(period);
    
    startConv = 1;
    filterCol = 2;
    filterRow = 2;
    imageCol = 5;
    imageRow = 5;
    stride = 1;
    f = 2;
    s = 8;
    No = 8;
    #(period);
    
    startConv = 0;
    
    #(1000*period);
    
    $finish();
end

initial begin
  $dumpfile("CSCConv2D.vcd");
  $dumpvars;
end

endmodule
