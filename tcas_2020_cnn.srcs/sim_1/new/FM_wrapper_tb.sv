`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UMBC-EEHPC
// Engineer: Nitheesh Manjunath  
// 
// Create Date: 05/05/2020 01:01:11 PM
// Design Name: 
// Module Name: FM_wrapper_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module FM_wrapper_tb#(
    dw = 8,
    aw = 4,
    PE = 4);
    
reg clk;
reg rst_n;
reg [PE*dw-1:0] Input_1;
reg [aw-1:0] writeAddress;
reg nextMemoryWrite;
reg write;
reg writeAll;
reg read;
reg [PE*aw-1:0] readAddress;

wire [PE*dw-1:0] Output_1;
reg [dw-1:0] x;

integer period = 10, i, j;

FM_wrapper#(
    .dw(dw),
    .aw(aw),
    .PE(PE),
    .memFile("")
    )uut(
    clk, rst_n, Input_1, writeAddress, nextMemoryWrite, write, writeAll, read, readAddress, Output_1
    );

initial begin
    clk = 0;
    forever #(period/2) clk = ~clk;
end


initial begin
    rst_n = 0;
    Input_1 = 0;
    writeAddress = 0;
    nextMemoryWrite = 0;
    write = 0;
    writeAll = 0;
    read = 0;
    readAddress = 0;
    #(period);
    
    rst_n = 1;
    #(period);
    
    for(j=0;j<PE; j++) begin
        nextMemoryWrite = 1;
        write = 1;
        #(period);
        nextMemoryWrite = 0;
        for(i=0;i< (1<<aw); i++) begin
            x  = $random;
            Input_1 = {4{x}};
            writeAddress = i;
            write = 1;
            #(period);
        end
        #(2*period);

    end
    
    write = 0;
    
    for(i=0;i<(1<<aw); i++) begin
        read = 1;
        readAddress[(PE*aw-1):(PE-1)*aw] = i;
        readAddress[((PE-1)*aw-1):(PE-2)*aw] = i;
        readAddress[((PE-2)*aw-1):(PE-3)*aw] = i;
        readAddress[((PE-3)*aw-1):(PE-4)*aw] = i;
        #(period);
    end
    read = 0;
    #(period);
//    $finish();
    
    for(i=0;i< (1<<aw); i++) begin
        Input_1 = $random;
        writeAddress = i;
        writeAll = 1;
        #(period);
    end
    
    $finish();
    
end

initial begin
  $dumpfile("FM_wrapper.vcd");
  $dumpvars;
end

endmodule
