`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/03/2020 04:33:56 PM
// Design Name: 
// Module Name: MAC_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MAC_tb(

    );
parameter dataWidth_i = 8;
parameter dataWidth_o = 16;

reg clk, reset;
reg [dataWidth_i-1:0] A, B;

wire [dataWidth_o-1:0] P;

MAC uut(
    .clk(clk),
    .reset(reset),
    
    .A(A),
    .B(B),
    
    .P(P));

integer period = 10, i = 0;

initial begin
    clk = 0;
    forever #(period/2) clk = ~clk;
end

initial begin
    reset = 1;
    A = 0;
    B = 0;
    #(period);
    
    reset = 0;
    #(period);
    
    for(i=0; i<5; i=i+1) begin
        A = i;
        B = i;
        #(period);
    end
    
    reset = 1;
    #(period);
    
    reset = 0;
    for(i=0; i<5; i=i+1) begin
        A = i+1;
        B = i-1;
        #(period);
    end
    
    
end
endmodule
