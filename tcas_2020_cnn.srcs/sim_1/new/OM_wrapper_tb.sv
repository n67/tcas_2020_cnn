`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/11/2020 12:14:11 PM
// Design Name: 
// Module Name: OM_wrapper_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module OM_wrapper_tb#(
        dw = 32,
        aw = 8,
        PE = 4,
        memFile = ""
        );
    
reg clk;
reg rst_n;
reg [PE*dw-1:0] Input_1;
reg firstConv;
reg nextRow, nextCol;
reg write;
reg read;
reg convDone;
wire [PE*dw-1:0] Output_1;
wire doneLoading;

integer period = 10, i, j;

OM_wrapper#(
    .dw(dw),
    .aw(aw),
    .PE(PE),
    .memFile(memFile)
    )uut(
    clk, 
    rst_n, 
    Input_1, 
    firstConv, 
    nextRow,
    nextCol, 
    write, 
    read, 
    convDone,
    Output_1, 
    doneLoading
    );
        
initial begin
    clk = 1;
    forever #(period/2) clk = ~clk;
end

initial begin
rst_n = 0;
Input_1 = 0;
firstConv = 0;
nextRow = 0;
nextCol = 0;
write = 0;
read = 0;
convDone = 0;
#(3*period);

rst_n = 1;
#(period);

firstConv = 1;
for(i=0; i<32; i++) begin
    read = 1;
    write = 0;
    #(period);
    read = 0;
    #(period);
    @(posedge clk) Input_1 = Output_1 + i;
    write = 1;
    #(period);
end
#(period*5);

//nextRow = 1;
nextCol = 1;
firstConv = 0;
#(period);

nextCol = 0;
nextRow = 0;
for(i=0; i<32; i++) begin
    read = 1;
    write = 0;
    #(period);
    read = 0;
    #(period);
    @(posedge clk) Input_1 = Output_1 + i;
    write = 1;
    #(period);
end
#(period*5);

nextRow = 0;
nextCol = 1;
firstConv = 0;
#(period);

nextCol = 0;
nextRow = 0;
for(i=0; i<32; i++) begin
    read = 1;
    write = 0;
    #(period);
    read = 0;
    #(period);
    @(posedge clk) Input_1 = Output_1 + i;
    write = 1;
    #(period);
end
#(period*5);

convDone = 1;
while(!doneLoading) begin
    #(period);
end

#(2*period);

$finish();
end

initial begin
  $dumpfile("OM_wrapper.vcd");
  $dumpvars;
end

endmodule
